FROM java:8
COPY build/libs/score-service-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar score-service-1.0-SNAPSHOT.jar