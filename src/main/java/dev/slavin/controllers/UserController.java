package dev.slavin.controllers;

import dev.slavin.models.User;
import dev.slavin.services.UserService;

import dev.slavin.util.ErrorLogger;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;

public class UserController {
    private static final String AUTH_HEADER = "Authorization";
    private static final String INVALID_USER = "That is not a valid user.";
    private static final String INVALID_USER_ID = " is not a valid user id.";
    private static final String INVALID_USERNAME = " is not a valid username.";
    private static final String UNAUTHORIZED = "You are unauthorized.";
    private static final String GENERAL = "general-auth-token";
    private static final String ADMIN = "admin-auth-token";

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final ErrorLogger errorLogger = new ErrorLogger(UserController.class, logger);

    private UserService userService = new UserService();
    
    private User user = new User();

    public void logIn(Context ctx) {
        String userName = ctx.formParam("userName");  //(Content-Type: application/x-www-form-urlencoded)
        String password = ctx.formParam("password");
        if (userName != null && userService.getUserByUserName(userName) != null) {
            try {
                user = userService.getUserByUserName(userName);
                if (password != null && password.equals(user.getPassword())) {
                    if (user.getAuthLevel() == 1) {
                        ctx.header(AUTH_HEADER, GENERAL);
                        ctx.header("access-control-expose-headers", AUTH_HEADER);
                        return;
                    }
                    if (user.getAuthLevel() == 2) {
                        ctx.header(AUTH_HEADER, ADMIN);
                        ctx.header("access-control-expose-headers", AUTH_HEADER);
                        return;
                    }
                    throw new NoSuchElementException("There was an unexpected error logging in.");
                }
            } catch (Exception e) {
                logger.warn("Failed login attempt");
                throw new UnauthorizedResponse("Username/password not recognized.");
            }
        }
        throw new UnauthorizedResponse("Username/password not recognized.");
    }

    public void isLoggedIn(Context ctx) {
        if (ctx.method().equals("OPTIONS")) {
            return;
        }

        if (ctx.header(AUTH_HEADER) == null) {
            throw new ForbiddenResponse("You must log in first.");
        }
    }

    public void adminAuth(Context ctx) {
        if (ctx.method().equals("OPTIONS")) {
            return;
        }

        String authHeader = ctx.header(AUTH_HEADER);
        if (authHeader == null || !authHeader.equals(ADMIN)) {
            throw new UnauthorizedResponse(UNAUTHORIZED);
        }
    }

    public void handleGetUsers(Context ctx) {
        String userName = ctx.queryParam("username");
        if (userName != null) {
            try {
                User user1 = userService.getUserByUserName(userName);
                int id = user1.getId();
                ctx.redirect("users/" + id);
            } catch (Exception e) {
                errorLogger.logError(e);
                throw new BadRequestResponse(userName + INVALID_USERNAME);
            }
        } else {
            ctx.json(userService.getAllUsers());
        }
    }

    public void handleGetUserById(Context ctx) {
        String pathParamId = ctx.pathParam("id");
        try {
           int id = Integer.parseInt(pathParamId);
           ctx.json(userService.getUser(id));
        } catch (Exception e) {
            errorLogger.logError(e);
            throw new BadRequestResponse(pathParamId + INVALID_USER_ID);
        }
    }

    public void handleAddNewUser(Context ctx) {
        try {
            user = ctx.bodyAsClass(User.class);
            ctx.json(userService.addUser(user));
            ctx.status(201);
        } catch (Exception e) {
            errorLogger.logError(e);
            throw new BadRequestResponse(INVALID_USER);
        }
    }

    public void handleUpdateUser(Context ctx) {
        try {
        user = ctx.bodyAsClass(User.class);
            userService.updateUser(user);
            ctx.status(201);
        } catch (Exception e) {
            errorLogger.logError(e);
            throw new BadRequestResponse(INVALID_USER);
        }
    }

    public void handleDeleteUser(Context ctx) {
        String pathParamId = "";
        try {
            pathParamId = ctx.pathParam("id");
            int id = Integer.parseInt(pathParamId);
           userService.deleteUser(id);
           ctx.status(204);
        } catch (Exception e) {
            errorLogger.logError(e);
            throw new BadRequestResponse(pathParamId + INVALID_USER_ID);
        }
    }
}
