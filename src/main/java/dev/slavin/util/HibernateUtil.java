package dev.slavin.util;

import dev.slavin.models.Composer;
import dev.slavin.models.Composition;
import dev.slavin.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private HibernateUtil() {}

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
//            settings.put(Environment.URL, System.getenv("URL"));
//            settings.put(Environment.USER, System.getenv("USER"));
//            settings.put(Environment.PASS, System.getenv("PASS"));

//            settings.put(Environment.URL, System.getenv("PostgresLocalConnectionString"));
//            settings.put(Environment.USER, System.getenv("PostgresLocalUserName"));
//            settings.put(Environment.PASS, System.getenv("PostgresLocalPassword"));

            settings.put(Environment.URL, "jdbc:sqlserver://pslavin-training.database.windows.net:1433;database=pslavin-trainingdb");
            settings.put(Environment.USER, "pslavin@pslavin-training");
            settings.put(Environment.PASS, "Ph0lo0e0");

//            settings.put(Environment.DRIVER, "org.postgresql.Driver");
//            settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");

            //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            configuration.addAnnotatedClass(Composition.class);
            configuration.addAnnotatedClass(Composer.class);
            configuration.addAnnotatedClass(User.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession() { return getSessionFactory().openSession(); }
}
