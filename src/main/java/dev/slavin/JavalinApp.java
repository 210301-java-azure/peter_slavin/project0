package dev.slavin;

import dev.slavin.controllers.ComposerController;
import dev.slavin.controllers.CompositionController;
import dev.slavin.controllers.UserController;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    CompositionController compositionController = new CompositionController();
    ComposerController composerController = new ComposerController();
    UserController userController = new UserController();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {
        before(ctx -> {
            if (!ctx.path().equals("/login")) {
                userController.isLoggedIn(ctx); // checks for ANY "Authorization" token
            }
        });
        before(ctx -> {
            if (!ctx.method().equals("GET") && !ctx.path().equals("/login")) {
                userController.adminAuth(ctx); // checks for admin token
            }
        });
        path("login", () -> post(userController::logIn));
        path("users", () -> {
            before("/", userController::adminAuth); // checks for admin token
            get(userController::handleGetUsers);
            post(userController::handleAddNewUser);
            path(":id", () -> {
                before("/", userController::adminAuth); // checks for admin token
                get(userController::handleGetUserById);
                put(userController::handleUpdateUser);
                delete(userController::handleDeleteUser);
            });
        });
        path("composers", () -> {
            before("/", userController::isLoggedIn);
            get(composerController::handleGetAllComposers);
            post(composerController::handleAddNewComposer);
            path(":id", () -> {
                before("/", userController::isLoggedIn);
                get(composerController::handleGetComposerById);
                put(composerController::handleUpdateComposer);
                delete(composerController::handleDeleteComposer);
                path("compositions", () -> get(compositionController::handleGetCompositionsByComposer));
            });
        });
        path("compositions", () -> {
            before("/", userController::isLoggedIn);
            get(compositionController::handleGetAllCompositions);
            post(compositionController::handleAddNewComposition);
            path(":id", () -> {
                before("/", userController::isLoggedIn);
                get(compositionController::handleGetCompositionById);
                put(compositionController::handleUpdateComposition);
                delete(compositionController::handleDeleteComposition);
            });
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
