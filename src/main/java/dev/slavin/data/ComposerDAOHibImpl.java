package dev.slavin.data;

import dev.slavin.models.Composer;
import dev.slavin.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ComposerDAOHibImpl implements ComposerDAO {
    private final Logger logger = LoggerFactory.getLogger(ComposerDAOHibImpl.class);

    @Override
    public List<Composer> getAllComposers() {
        try (Session s = HibernateUtil.getSession()) {
             return s.createQuery("from Composer", Composer.class).list();
        }
    }

    @Override
    public Composer getComposerById(int id) {
        try (Session s = HibernateUtil.getSession()) {
            return s.get(Composer.class, id);
        }
    }

    @Override
    public Composer addNewComposer(Composer composer) {
        try (Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            int id = (int) s.save(composer);
            composer.setId(id);
            logger.info("added new composer");
            return composer;
        }
    }

    @Override
    public void updateComposer(Composer composer) {
        Composer oldComposer = getComposerById(composer.getId());
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            oldComposer.setName(composer.getName());
            oldComposer.setBirthYear(composer.getBirthYear());
            oldComposer.setDeathYear(composer.getDeathYear());
            s.update(oldComposer);
            tx.commit();
        }
    }

    @Override
    public void deleteComposer(int id) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Composer(id));
            tx.commit();
        }
    }
}
