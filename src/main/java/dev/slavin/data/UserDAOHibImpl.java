package dev.slavin.data;

import dev.slavin.models.User;
import dev.slavin.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserDAOHibImpl implements UserDAO {
    private final Logger logger = LoggerFactory.getLogger(UserDAOHibImpl.class);

    @Override
    public List<User> getAllUsers() {
        try (Session s = HibernateUtil.getSession()) {
            return s.createQuery("from User", User.class).list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session s = HibernateUtil.getSession()) {
            return s.get(User.class, id);
        }
    }

    @Override
    public User getUserByUserName(String userName) {
        try (Session s = HibernateUtil.getSession()) {
            Query<User> userQuery = s.createQuery("from User where userName = :userName", User.class);
            userQuery.setParameter("userName", userName);
            return userQuery.getSingleResult();
        }
    }

    @Override
    public User addNewUser(User user) {
        try (Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            int id = (int) s.save(user);
            user.setId(id);
            logger.info("added new user");
            return user;
        }
    }

    @Override
    public void updateUser(User user) {
        int id = user.getId();
        String password = user.getPassword();
        int authLevel = user.getAuthLevel();
        try (Session s = HibernateUtil.getSession()) {
            Query<User> userQuery = s.createQuery("update User set password = :password, authLevel = :authLevel where id = :id", User.class);
            userQuery.setParameter("id", id);
            userQuery.setParameter("password", password);
            userQuery.setParameter("authLevel", authLevel);
        }
    }

    @Override
    public void deleteUser(int id) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new User(id));
            tx.commit();
        }
    }

    @Override
    public User logIn(String userName, String password) {
        User user = new User();
        if (userName != null && password != null) {
            user = getUserByUserName(userName);
        }
        return user;
    }
}
