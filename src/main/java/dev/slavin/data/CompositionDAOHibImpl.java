package dev.slavin.data;

import dev.slavin.models.Composer;
import dev.slavin.models.Composition;
import dev.slavin.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CompositionDAOHibImpl implements CompositionDAO {
    private final Logger logger = LoggerFactory.getLogger(CompositionDAOHibImpl.class);

    @Override
    public List<Composition> getAllCompositions() {
        try (Session s = HibernateUtil.getSession()) {
            return s.createQuery("from Composition", Composition.class).list();
        }
    }

    @Override
    public Composition getCompositionById(int id) {
        try (Session s = HibernateUtil.getSession()) {
            return s.get(Composition.class, id);
        }
    }

    @Override
    public List<Composition> getCompositionsByComposer(Composer composer) {
        try (Session s = HibernateUtil.getSession()) {
            Query<Composition> compositionQuery = s.createQuery("from Composition where composer = :composer", Composition.class);
            compositionQuery.setParameter("composer", composer);
            return compositionQuery.list();
        }
    }

    @Override
    public Composition addNewComposition(Composition composition) {
        try (Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            int id = (int) s.save(composition);
            composition.setId(id);
            logger.info("added new composition");
            return composition;
        }
    }

    @Override
    public void updateComposition(Composition composition) {
        int id = composition.getId();
        String title = composition.getTitle();
        Composer composer = composition.getComposer();
        int yearComposed = composition.getYearComposed();
        String genre = composition.getGenre();
        Boolean multiMovement = composition.getMultiMovement();
        try (Session s = HibernateUtil.getSession()) {
            Query<Composition> compositionQuery = s.createQuery("update Composition set title = :title, composerId = :composerId," +
                    "yearComposed = :yearComposed, genre = :genre, multiMovement = :multiMovement where id = :id", Composition.class);
            compositionQuery.setParameter("id", id);
            compositionQuery.setParameter("title", title);
            compositionQuery.setParameter("composerId", composer);
            compositionQuery.setParameter("yearComposed", yearComposed);
            compositionQuery.setParameter("genre", genre);
            compositionQuery.setParameter("multiMovement", multiMovement);
        }
    }

    @Override
    public void deleteComposition(int id) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Composition(id));
            tx.commit();
        }
    }
}
