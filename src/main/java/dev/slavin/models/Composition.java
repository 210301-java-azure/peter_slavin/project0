package dev.slavin.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "composition")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Composition implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    @Column(name = "year_composed")
    private int yearComposed;

    private String genre;

    @Column(name = "multi_movement")
    private Boolean multiMovement;

    @ManyToOne
    private Composer composer;

    public Composition() {
        super();
    }

    public Composition(int id) {
        this.id = id;
    }

    public Composition(String title, Composer composer, int yearComposed, String genre, Boolean multiMovement) {
        this.title = title;
        this.composer = composer;
        this.yearComposed = yearComposed;
        this.genre = genre;
        this.multiMovement = multiMovement;
    }

    public Composition(int id, String title, Composer composer, int yearComposed, String genre, Boolean multiMovement) {
        this.id = id;
        this.title = title;
        this.composer = composer;
        this.yearComposed = yearComposed;
        this.genre = genre;
        this.multiMovement = multiMovement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Composer getComposer() {
        return composer; }

    public void setComposer(Composer composer) {
        this.composer = composer; }

    public int getYearComposed() {
        return yearComposed;
    }

    public void setYearComposed(int yearComposed) {
        this.yearComposed = yearComposed;
    }

    public String getGenre() {
        return genre; }

    public void setGenre(String genre) {
        this.genre = genre; }

    public boolean getMultiMovement() {
        return multiMovement;
    }

    public void setMultiMovement(Boolean multiMovement) {
        this.multiMovement = multiMovement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Composition that = (Composition) o;
        return id == that.id && multiMovement == that.multiMovement && Objects.equals(title, that.title) && Objects.equals(composer, that.composer) && Objects.equals(yearComposed, that.yearComposed) && genre == that.genre;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, composer, yearComposed, genre, multiMovement);
    }

    @Override
    public String toString() {
        return "Composition{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", composerId='" + composer + '\'' +
                ", yearComposed=" + yearComposed +
                ", genre=" + genre +
                ", multiMovement=" + multiMovement +
                '}';
    }
}