package dev.slavin.controllers;

import dev.slavin.models.Composer;
import dev.slavin.models.Composition;
import dev.slavin.services.CompositionService;
import io.javalin.http.Context;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

    class CompositionControllerTest {
    @InjectMocks
    private CompositionController CompositionController;

    @Mock
    private CompositionService service;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
        void getAllCompositionsCallsServiceMethod() {
        Context context = mock(Context.class);

        Composer acopland = new Composer();
        Composer gligeti = new Composer();

        List<Composition> compositions = new ArrayList<>();
        compositions.add(new Composition("Appalachian Spring",acopland, 1930, "ballet",false));
        compositions.add(new Composition("Le grand Macabre",gligeti, 1978, "opera", true));

        when(service.getAllCompositions()).thenReturn(compositions);
        CompositionController.handleGetAllCompositions(context);
        verify(context).json(compositions);
    }
}
