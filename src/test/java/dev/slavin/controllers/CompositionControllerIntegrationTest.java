package dev.slavin.controllers;

import dev.slavin.JavalinApp;
import dev.slavin.models.Composition;
import jdk.nashorn.internal.ir.annotations.Ignore;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CompositionControllerIntegrationTest {
    private static JavalinApp app = new JavalinApp();

    private static final String AUTH_TOKEN = "Authorization";
    private static final String GENERAL = "general-auth-token";
    private static final String ADMIN = "admin-auth-token";
    private static final String PATH = "http://13.90.159.78/compositions";
    private static final String COMPOSERS_PATH = "http://13.90.159.78/composers";
    private static final String INVALID_COMPOSITION = "That is not a valid composition.";
    private static final String UNAUTHORIZED = "You are unauthorized.";

    @BeforeAll
    static void startService(){
        app.start(80);
    }

    @AfterAll
    static void stopService(){
        app.stop();
    }

    @Test
    void getAllCompositionsPermitsAnyAndFetchesList() {
        HttpResponse<List<Composition>> response = Unirest.get(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asObject(new GenericType<List<Composition>>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(response.getBody().size() > 0)
        );
    }

    @Test
    void getPermitsAnyAndFetchesComposition() {
        HttpResponse<Composition> response = Unirest.get(PATH + "/3")
                .header(AUTH_TOKEN, GENERAL)
                .asObject(new GenericType<Composition>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertNotNull(response.getBody())
        );
    }

    @Test
    void getByComposerPermitsAnyAndFetchesList() {
        HttpResponse<List<Composition>> response = Unirest.get(COMPOSERS_PATH + "/1/compositions")
                .header(AUTH_TOKEN, GENERAL)
                .asObject(new GenericType<List<Composition>>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(response.getBody().size() > 0)
        );
    }

//    @Test
//    void postCompositionProhibitsUnauthorized() {
//        HttpResponse<String> response = Unirest.post(PATH)
//                .header(AUTH_TOKEN, GENERAL)
//                .asString();
//        assertAll(
//                () -> assertEquals(401, response.getStatus()),
//                () -> assertEquals(UNAUTHORIZED, response.getBody()));
//    }

    @Test
    void postCompositionPermitsAuthorized() {
        HttpResponse<String> response = Unirest.post(PATH)
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(INVALID_COMPOSITION, response.getBody()));
    }

//    @Test
//    void putCompositionProhibitsUnauthorized() {
//        HttpResponse<String> response = Unirest.put(PATH)
//                .header(AUTH_TOKEN, GENERAL)
//                .asString();
//        assertAll(
//                () -> assertEquals(401, response.getStatus()),
//                () -> assertEquals(UNAUTHORIZED, response.getBody()));
//    }

    @Test
    void putCompositionPermitsAuthorized() {
        HttpResponse<String> response = Unirest.put(PATH + "/1")
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(INVALID_COMPOSITION, response.getBody()));
    }

//    @Test
//    void deleteCompositionProhibitsUnauthorized() {
//        HttpResponse<String> response = Unirest.delete(PATH + "/0")
//                .header(AUTH_TOKEN, GENERAL)
//                .asString();
//        assertAll(
//                () -> assertEquals(401, response.getStatus()),
//                () -> assertEquals(UNAUTHORIZED, response.getBody()));
//    }

    @Test
    void deleteCompositionPermitsAuthorized() {
        HttpResponse<String> response = Unirest.delete(PATH + "/0")
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals(204, response.getStatus()),
                () -> assertEquals("", response.getBody()));
    }
}
