package dev.slavin.controllers;

import dev.slavin.JavalinApp;
import dev.slavin.models.User;
import jdk.nashorn.internal.ir.annotations.Ignore;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

 class UserControllerIntegrationTest {
    private static JavalinApp app = new JavalinApp();

     private static final String AUTH_TOKEN = "Authorization";
     private static final String GENERAL = "general-auth-token";
     private static final String ADMIN = "admin-auth-token";
     private static final String PATH = "http://13.90.159.78/users";
     private static final String UNAUTHORIZED = "You are unauthorized.";
     private static final String INVALID = "That is not a valid user.";

    @BeforeAll
     static void startService(){
        app.start(7000);
    }

    @AfterAll
     static void stopService(){
        app.stop();
    }

    @Test
     void getAllUsersProhibitsUnauthorized() {
        HttpResponse<String> response = Unirest.get(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asString();
        assertAll(
                () -> assertEquals( 401, response.getStatus()),
                () -> assertEquals( UNAUTHORIZED, response.getBody()));
    }

    @Test
     void getAllUsersPermitsAuthorized() {
        HttpResponse<List<User>> response = Unirest.get(PATH)
                .header(AUTH_TOKEN, ADMIN)
                .asObject(new GenericType<List<User>>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(response.getBody().size() > 0)
        );
    }

    @Test
     void getUserProhibitsUnauthorized() {
        HttpResponse<String> response = Unirest.get(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asString();
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(UNAUTHORIZED, response.getBody())
        );
    }

    @Test
     void getUserPermitsAuthorized() {
        HttpResponse<User> response = Unirest.get(PATH + "/1")
                .header(AUTH_TOKEN, ADMIN)
                .asObject(new GenericType<User>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertNotNull(response.getBody())
        );
    }

     @Test
     void postUserProhibitsUnauthorized() {
         HttpResponse<String> response = Unirest.post(PATH)
                 .header(AUTH_TOKEN, GENERAL)
                 .asString();
         assertAll(
                 () -> assertEquals(401, response.getStatus()),
                 () -> assertEquals(UNAUTHORIZED, response.getBody()));
     }

     @Test
     void postUsersPermitsAuthorized() {
         HttpResponse<String> response = Unirest.post(PATH)
                 .header(AUTH_TOKEN, ADMIN)
                 .asString();
         assertAll(
                 () -> assertEquals(400, response.getStatus()),
                 () -> assertEquals(INVALID, response.getBody()));
     }

     @Test
     void putUserProhibitsUnauthorized() {
         HttpResponse<String> response = Unirest.put(PATH)
                 .header(AUTH_TOKEN, GENERAL)
                 .asString();
         assertAll(
                 () -> assertEquals(401, response.getStatus()),
                 () -> assertEquals(UNAUTHORIZED, response.getBody()));
     }

     @Test
     void putUserPermitsAuthorized() {
         HttpResponse<String> response = Unirest.put(PATH + "/1")
                 .header(AUTH_TOKEN, ADMIN)
                 .asString();
         assertAll(
                 () -> assertEquals(400, response.getStatus()),
                 () -> assertEquals(INVALID, response.getBody()));
     }

     @Test
     void deleteUserProhibitsUnauthorized() {
         HttpResponse<String> response = Unirest.delete(PATH + "/0")
                 .header(AUTH_TOKEN, GENERAL)
                 .asString();
         assertAll(
                 () -> assertEquals(401, response.getStatus()),
                 () -> assertEquals(UNAUTHORIZED, response.getBody()));
     }

     @Test
     void deleteUserPermitsAuthorized() {
         HttpResponse<String> response = Unirest.delete(PATH + "/0")
                 .header(AUTH_TOKEN, ADMIN)
                 .asString();
         assertAll(
                 () -> assertEquals( 204, response.getStatus()),
                 () -> assertEquals( "", response.getBody()));
     }
 }
