# SCORE SERVICE

## Project Description

This is a RESTful service and UI that allows registered users to view tables listing composers and their compositions.

## Technologies Used

* Java 8
* Hibernate
* Javalin
* MS SQL Server
* HTML/CSS/Javascript

## Features

* Login
* Listing resources
* Getting a user by username
* Adding composers and compositions
* Deleting compositions.

To-do list

* Updating resources
* Getting lists of compositions by composer
* Deleting composers along with their related compositions

## Usage

A user who is in the database, with a username and password, can log in. If users have administrative privileges, they may also add and delete compositions, or add composers, which must be present in the database for compositions by them to be added. Admin users may also view data pertaining to any registered user, accessible by username.



